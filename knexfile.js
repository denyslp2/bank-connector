require('dotenv').config();

module.exports = {
  development: {
    client: 'pg',
    connection: {
      database: process.env.DATABASE,
      user: process.env.PG_USER,
      password: process.env.PASSWORD,
      host: process.env.DEV_HOST,
      port: process.env.PG_PORT,
    },
    migrations: {
      directory: __dirname + '/db/migrations',
    },
    seeds: {
      directory: __dirname + '/db/seeds',
    },
  },
  local_docker_db: {
    client: 'pg',
    connection: {
      database: process.env.DATABASE,
      user: process.env.PG_USER,
      password: process.env.PASSWORD,
      host: process.env.LOCAL_HOST,
      port: process.env.PG_PORT,
    },
    migrations: {
      directory: __dirname + '/db/migrations',
    },
    seeds: {
      directory: __dirname + '/db/seeds',
    },
  },
};

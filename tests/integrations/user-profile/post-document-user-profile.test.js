const supertest = require('supertest');

const Server = require('../../../src/infrastructure/server');
const { logger } = require('../../../src/infrastructure/utils');
const { nocks } = require('../../utils');

let server = new Server();
let app;
let request;

const TOKEN =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.dk8cVwBMdjmCEn_Q6s2qveecaoGEGIOkcQAwXkGBeK4';

describe('Integration tests POST post-document-user-profile', () => {
  beforeAll(async () => {
    app = server.app;
    request = await supertest(app.callback());
  });

  beforeEach(() => {
    nocks.cleanAll();
    jest.clearAllMocks();
  });

  afterAll(async () => {
    await server.stop();
  });

  describe('Success cases', () => {
    it('200, Should send to user-profile user-bank-details', async () => {
      const provider = 'belvo';
      const userUuid = 'uuid';

      const bodyUserBankDetails = {
        to: 'bank-connector',
        retry: 0,
        data: {
          userUuid,
          owner_data: [
            {
              link: 'uuid',
            },
          ],
          transactions_data: [
            {
              value_date: '2021-10-01',
            },
          ],
        },
      };

      const nockPostUserProfile =
        nocks.userProfile.incomeDocument.postUserProfile({
          provider,
          userUuid,
          responseBody: 'Ok',
        });

      const responseOk = { message: 'success' };

      const { body: responseBody } = await request
        .post('/save-user-bank-details')
        .set('Authorization', `Bearer ${TOKEN}`)
        .send(bodyUserBankDetails)
        .expect(200);

      expect(responseBody).toStrictEqual(responseOk);
      expect(nockPostUserProfile.isDone()).toBeTruthy();
    });
    it('200, Should send to user-profile user-bank-details with owner_data empty', async () => {
      const provider = 'belvo';
      const userUuid = 'uuid';
      const bodyUserBankDetails = {
        to: 'bank-connector',
        retry: 0,
        data: {
          userUuid,
          owner_data: [],
          transactions_data: [
            {
              value_date: '2021-10-01',
            },
          ],
        },
      };

      const nockPostUserProfile =
        nocks.userProfile.incomeDocument.postUserProfile({
          provider,
          userUuid,
          responseBody: 'Ok',
        });

      const responseOk = { message: 'success' };

      const { body: responseBody } = await request
        .post('/save-user-bank-details')
        .set('Authorization', `Bearer ${TOKEN}`)
        .send(bodyUserBankDetails)
        .expect(200);

      expect(responseBody).toStrictEqual(responseOk);
      expect(nockPostUserProfile.isDone()).toBeTruthy();
    });
    it('200, Should send to user-profile user-bank-details with transactions_data empty', async () => {
      const provider = 'belvo';
      const userUuid = 'uuid';
      const bodyUserBankDetails = {
        to: 'bank-connector',
        retry: 0,
        data: {
          userUuid,
          owner_data: [
            {
              link: 'uuid',
            },
          ],
          transactions_data: [],
        },
      };

      const nockPostUserProfile =
        nocks.userProfile.incomeDocument.postUserProfile({
          provider,
          userUuid,
          responseBody: 'Ok',
        });

      const responseOk = { message: 'success' };

      const { body: responseBody } = await request
        .post('/save-user-bank-details')
        .set('Authorization', `Bearer ${TOKEN}`)
        .send(bodyUserBankDetails)
        .expect(200);

      expect(responseBody).toStrictEqual(responseOk);
      expect(nockPostUserProfile.isDone()).toBeTruthy();
    });
    it('200, Should send to user-profile user-bank-details with owner_data and transactions_data empty', async () => {
      const provider = 'belvo';
      const userUuid = 'uuid';
      const bodyUserBankDetails = {
        to: 'bank-connector',
        retry: 0,
        data: {
          userUuid,
          owner_data: [],
          transactions_data: [],
        },
      };

      const nockPostUserProfile =
        nocks.userProfile.incomeDocument.postUserProfile({
          provider,
          userUuid,
          responseBody: 'Ok',
        });

      const responseOk = { message: 'success' };

      const { body: responseBody } = await request
        .post('/save-user-bank-details')
        .set('Authorization', `Bearer ${TOKEN}`)
        .send(bodyUserBankDetails)
        .expect(200);

      expect(responseBody).toStrictEqual(responseOk);
      expect(nockPostUserProfile.isDone()).toBeTruthy();
    });
  });
  describe('Error cases', () => {
    it('401, should not be authorized send to user-profile if the token is not provided', async () => {
      const bodyUserBankDetails = {
        to: 'bank-connector',
        retry: 0,
        data: {
          userUuid: 'uuid',
          owner_data: [],
          transactions_data: [],
        },
      };

      await request
        .post('/save-user-bank-details')
        .send(bodyUserBankDetails)
        .expect(401);
    });
    it('400, Should throw error with property to is not provider', async () => {
      const bodyUserBankDetails = {
        retry: 0,
        data: {
          userUuid: 'uuid',
          owner_data: [],
          transactions_data: [],
        },
      };

      const messageLogError = `Error to validate schema from request: {"retry":0,"data":{"userUuid":"uuid","owner_data":[],"transactions_data":[]}} - error: must have required property 'to'`;
      const spyLoggerError = jest.spyOn(logger, 'error').mockImplementation();

      await request
        .post('/save-user-bank-details')
        .set('Authorization', `Bearer ${TOKEN}`)
        .send(bodyUserBankDetails)
        .expect(400);

      expect(spyLoggerError).toHaveBeenCalledTimes(1);
      expect(spyLoggerError.mock.calls[0].toString()).toContain(
        messageLogError
      );
      spyLoggerError.mockRestore();
    });
    it('400, Should throw error with property retry is not provider', async () => {
      const bodyUserBankDetails = {
        to: 'bank-connector',
        data: {
          userUuid: 'uuid',
          owner_data: [],
          transactions_data: [],
        },
      };

      const messageLogError = `Error to validate schema from request: {"to":"bank-connector","data":{"userUuid":"uuid","owner_data":[],"transactions_data":[]}} - error: must have required property 'retry'`;
      const spyLoggerError = jest.spyOn(logger, 'error').mockImplementation();

      await request
        .post('/save-user-bank-details')
        .set('Authorization', `Bearer ${TOKEN}`)
        .send(bodyUserBankDetails)
        .expect(400);

      expect(spyLoggerError).toHaveBeenCalledTimes(1);
      expect(spyLoggerError.mock.calls[0].toString()).toContain(
        messageLogError
      );
      spyLoggerError.mockRestore();
    });
    it('400, Should throw error with property retry is not a number', async () => {
      const bodyUserBankDetails = {
        to: 'bank-connector',
        retry: '0',
        data: {
          userUuid: 'uuid',
          owner_data: [],
          transactions_data: [],
        },
      };

      const messageLogError =
        'Error to validate schema from request: {"to":"bank-connector","retry":"0","data":{"userUuid":"uuid","owner_data":[],"transactions_data":[]}} - error: must be number';
      const spyLoggerError = jest.spyOn(logger, 'error').mockImplementation();

      await request
        .post('/save-user-bank-details')
        .set('Authorization', `Bearer ${TOKEN}`)
        .send(bodyUserBankDetails)
        .expect(400);

      expect(spyLoggerError).toHaveBeenCalledTimes(1);
      expect(spyLoggerError.mock.calls[0].toString()).toContain(
        messageLogError
      );
      spyLoggerError.mockRestore();
    });
    it('400, Should throw error with property data is not provider', async () => {
      const bodyUserBankDetails = {
        to: 'bank-connector',
        retry: '0',
      };

      const messageLogError = `Error to validate schema from request: {"to":"bank-connector","retry":"0"} - error: must have required property 'data'`;
      const spyLoggerError = jest.spyOn(logger, 'error').mockImplementation();

      await request
        .post('/save-user-bank-details')
        .set('Authorization', `Bearer ${TOKEN}`)
        .send(bodyUserBankDetails)
        .expect(400);

      expect(spyLoggerError).toHaveBeenCalledTimes(1);
      expect(spyLoggerError.mock.calls[0].toString()).toContain(
        messageLogError
      );
      spyLoggerError.mockRestore();
    });
    it('400, Should throw error with property userUuid is not provider', async () => {
      const bodyUserBankDetails = {
        to: 'bank-connector',
        retry: '0',
        data: {
          owner_data: [],
          transactions_data: [],
        },
      };

      const messageLogError =
        'Error to validate schema from request: {"to":"bank-connector","retry":"0","data":{"owner_data":[],"transactions_data":[]}} - error: must be number';
      const spyLoggerError = jest.spyOn(logger, 'error').mockImplementation();

      await request
        .post('/save-user-bank-details')
        .set('Authorization', `Bearer ${TOKEN}`)
        .send(bodyUserBankDetails)
        .expect(400);

      expect(spyLoggerError).toHaveBeenCalledTimes(1);
      expect(spyLoggerError.mock.calls[0].toString()).toContain(
        messageLogError
      );
      spyLoggerError.mockRestore();
    });
    it('400, Should throw error with property userUuid is not a string', async () => {
      const bodyUserBankDetails = {
        to: 'bank-connector',
        retry: '0',
        data: {
          userUuid: 0,
          owner_data: [],
          transactions_data: [],
        },
      };

      const messageLogError =
        'Error to validate schema from request: {"to":"bank-connector","retry":"0","data":{"userUuid":0,"owner_data":[],"transactions_data":[]}} - error: must be number';
      const spyLoggerError = jest.spyOn(logger, 'error').mockImplementation();

      await request
        .post('/save-user-bank-details')
        .set('Authorization', `Bearer ${TOKEN}`)
        .send(bodyUserBankDetails)
        .expect(400);

      expect(spyLoggerError).toHaveBeenCalledTimes(1);
      expect(spyLoggerError.mock.calls[0].toString()).toContain(
        messageLogError
      );
      spyLoggerError.mockRestore();
    });
    it('400, Should throw error with property owner_data is not provider', async () => {
      const bodyUserBankDetails = {
        to: 'bank-connector',
        retry: '0',
        data: {
          userUuid: 'uuid',
          transactions_data: [],
        },
      };

      const messageLogError =
        'Error to validate schema from request: {"to":"bank-connector","retry":"0","data":{"userUuid":"uuid","transactions_data":[]}} - error: must be number';
      const spyLoggerError = jest.spyOn(logger, 'error').mockImplementation();

      await request
        .post('/save-user-bank-details')
        .set('Authorization', `Bearer ${TOKEN}`)
        .send(bodyUserBankDetails)
        .expect(400);

      expect(spyLoggerError).toHaveBeenCalledTimes(1);
      expect(spyLoggerError.mock.calls[0].toString()).toContain(
        messageLogError
      );
      spyLoggerError.mockRestore();
    });
    it('400, Should throw error with property transactions_data is not provider', async () => {
      const bodyUserBankDetails = {
        to: 'bank-connector',
        retry: '0',
        data: {
          userUuid: 'uuid',
          owner_data: [],
        },
      };

      const messageLogError =
        'Error to validate schema from request: {"to":"bank-connector","retry":"0","data":{"userUuid":"uuid","owner_data":[]}} - error: must be number';
      const spyLoggerError = jest.spyOn(logger, 'error').mockImplementation();

      await request
        .post('/save-user-bank-details')
        .set('Authorization', `Bearer ${TOKEN}`)
        .send(bodyUserBankDetails)
        .expect(400);

      expect(spyLoggerError).toHaveBeenCalledTimes(1);
      expect(spyLoggerError.mock.calls[0].toString()).toContain(
        messageLogError
      );
      spyLoggerError.mockRestore();
    });
  });
});

const ip = require('ip');
const supertest = require('supertest');

const packageInfo = require('../../../package.json');
const Server = require('../../../src/infrastructure/server');

const server = new Server();
let app;
let request;

describe('Integration tests health status', () => {
  beforeAll(async () => {
    app = server.app;
    request = await supertest(app.callback());
  });

  afterAll(async () => {
    await server.stop();
  });

  it('Should return 200 when call health-status', async () => {
    await request
      .get('/health-status')
      .set('Content-Type', 'application/json')
      .expect(200);
  });

  it('Should return the correct version in check-status', async () => {
    const response = await request
      .get('/health-status')
      .set('Content-Type', 'application/json')
      .expect(200);

    expect(response.statusCode).toBe(200);
    expect(response.body.service).toBe(packageInfo.name);
    expect(response.body.version).toBe(packageInfo.version);
    expect(response.body.container).toBe(process.env.HOSTNAME);
    expect(ip.isV4Format(response.body.ip)).toBe(true);
  });
});

const supertest = require('supertest');

const Server = require('../../../src/infrastructure/server');
let server = new Server();
let app;
let request;

const TOKEN =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.dk8cVwBMdjmCEn_Q6s2qveecaoGEGIOkcQAwXkGBeK4';

describe('Integration tests post process-bank-statement', () => {
  beforeAll(async () => {
    app = server.app;
    request = await supertest(app.callback());
  });

  afterAll(async () => {
    await server.stop();
  });

  describe('Success cases', () => {
    it('200, post data in queue', async () => {
      const linkId = 'uuid';
      const userUuid = 'uuid';
      const dateFrom = '2021-01-01';
      const dateTo = '2021-03-01';

      const bodyBankStatement = {
        credentials: {
          linkId,
          userUuid,
        },
        filters: {
          dateTo,
          dateFrom,
        },
      };

      await request
        .post('/process-bank-statement')
        .set('Authorization', `Bearer ${TOKEN}`)
        .send(bodyBankStatement)
        .expect(200);
    });
    it('200, Should get bank-statement without filter', async () => {
      const linkId = 'uuid';
      const dateFrom = '';
      const dateTo = '';
      const userUuid = 'uuid';

      const bodyBankStatement = {
        credentials: {
          linkId,
          userUuid,
        },
        filters: {
          dateTo,
          dateFrom,
        },
      };

      await request
        .post(`/process-bank-statement`)
        .set('Authorization', `Bearer ${TOKEN}`)
        .send(bodyBankStatement)
        .expect(200);
    });
  });
  describe('Error cases', () => {
    it('401, should not be authorized get bank-statement if the token is not provided', async () => {
      const linkId = 'uuid';
      const dateFrom = '2021-01-01';
      const userUuid = 'uuid';
      const dateTo = '2021-03-01';
      const bodyBankStatement = {
        credentials: {
          linkId,
          userUuid,
        },
        filters: {
          dateFrom,
          dateTo,
        },
      };

      await request
        .post(`/process-bank-statement`)
        .send(bodyBankStatement)
        .expect(401);
    });
    it('400, Should throw error with bodyBankStatement without credentials', async () => {
      const dateFrom = '2021-01-01';
      const dateTo = '2021-03-01';
      const bodyBankStatement = {
        filters: {
          dateFrom,
          dateTo,
        },
      };

      await request
        .post(`/process-bank-statement`)
        .set('Authorization', `Bearer ${TOKEN}`)
        .send(bodyBankStatement)
        .expect(400);
    });
    it('400, Should throw error with bodyBankStatement invalid dateFrom', async () => {
      const linkId = 'uuid';
      const dateTo = '2021-03-01';
      const dateFrom = 'banana';
      const userUuid = 'uuid';
      const bodyBankStatement = {
        credentials: {
          linkId,
          userUuid,
        },
        filters: {
          dateTo,
          dateFrom,
        },
      };

      await request
        .post(`/process-bank-statement`)
        .set('Authorization', `Bearer ${TOKEN}`)
        .send(bodyBankStatement)
        .expect(400);
    });
    it('400, Should throw error with bodyBankStatement invalid dateTo', async () => {
      const linkId = 'uuid';
      const dateFrom = '2021-03-01';
      const dateTo = 'Banana';
      const userUuid = 'uuid';
      const bodyBankStatement = {
        credentials: {
          linkId,
          userUuid,
        },
        filters: {
          dateFrom,
          dateTo,
        },
      };

      await request
        .post(`/process-bank-statement`)
        .set('Authorization', `Bearer ${TOKEN}`)
        .send(bodyBankStatement)
        .expect(400);
    });
    it('400, Should try get bank-statement without linkId.', async () => {
      const dateFrom = '2021-01-01';
      const dateTo = '2021-03-01';
      const userUuid = 'uuid';
      const bodyBankStatement = {
        credentials: {
          userUuid,
        },
        filters: {
          dateFrom,
          dateTo,
        },
      };

      await request
        .post(`/process-bank-statement`)
        .set('Authorization', `Bearer ${TOKEN}`)
        .send(bodyBankStatement)
        .expect(400);
    });
    it('400, Should try get bank-statement without userUuid.', async () => {
      const dateFrom = '2021-01-01';
      const dateTo = '2021-03-01';
      const linkId = 'uuid';
      const bodyBankStatement = {
        credentials: {
          linkId,
        },
        filters: {
          dateFrom,
          dateTo,
        },
      };

      await request
        .post(`/process-bank-statement`)
        .set('Authorization', `Bearer ${TOKEN}`)
        .send(bodyBankStatement)
        .expect(400);
    });
  });
});

require('dotenv').config();

const { sendToSqs } = require('../../../../../src/infrastructure/clients/sqs');
const { logger } = require('../../../../../src/infrastructure/utils');
const { nocks } = require('../../../../utils');

describe('Unit tests client retry-save-user-bank-details, Send to Queue', () => {
  beforeEach(() => {
    nocks.cleanAll();
    jest.clearAllMocks();
  });
  describe('Success cases', () => {
    it('Should send to queue with successful', async () => {
      const to = 'bank-connector';
      const retry = 1;
      const data = {
        owner_data: [],
        transactions_data: [],
      };

      const nockSendToSqs = nocks.retry.queue.sendToSqs();

      const result = await sendToSqs({
        to,
        retry,
        data,
      });

      expect(result.statusCode).toEqual(200);
      expect(nockSendToSqs.isDone()).toBeTruthy();
    });
  });
  describe('Error cases', () => {
    it('Should try retry send to queue with parameter to is undefined', async () => {
      const to = undefined;
      const retry = 1;
      const data = {
        owner_data: [],
        transactions_data: [],
      };

      const responseNockretrySaveUserBankDetails = {
        error: 'Input Parameter to not found.',
      };

      const nockSendToSqs = nocks.retry.queue.sendToSqs({
        statusCode: 400,
        errorMessage: responseNockretrySaveUserBankDetails,
      });

      const spyLoggerError = jest.spyOn(logger, 'error').mockImplementation();
      const messageLogError =
        'Error client retry save-user-bank-betails, error: ';

      const result = await sendToSqs({ to, retry, data });

      expect(result.error).toStrictEqual(
        responseNockretrySaveUserBankDetails.error
      );
      expect(result).toHaveProperty('error');
      expect(nockSendToSqs.isDone()).toBeFalsy();
      expect(spyLoggerError).toHaveBeenCalledTimes(1);
      expect(spyLoggerError.mock.calls[0].toString()).toContain(
        messageLogError
      );
      spyLoggerError.mockRestore();
    });
    it('Should try retry send to queue with parameter retry is undefined', async () => {
      const to = 'bank-connector';
      const retry = undefined;
      const data = {
        owner_data: [],
        transactions_data: [],
      };

      const responseNockretrySaveUserBankDetails = {
        error: 'Input Parameter retry not found.',
      };

      const nockSendToSqs = nocks.retry.queue.sendToSqs({
        statusCode: 400,
        errorMessage: responseNockretrySaveUserBankDetails,
      });

      const spyLoggerError = jest.spyOn(logger, 'error').mockImplementation();
      const messageLogError =
        'Error client retry save-user-bank-betails, error: ';

      const result = await sendToSqs({ to, retry, data });

      expect(result.error).toStrictEqual(
        responseNockretrySaveUserBankDetails.error
      );
      expect(result).toHaveProperty('error');
      expect(nockSendToSqs.isDone()).toBeFalsy();
      expect(spyLoggerError).toHaveBeenCalledTimes(1);
      expect(spyLoggerError.mock.calls[0].toString()).toContain(
        messageLogError
      );
      spyLoggerError.mockRestore();
    });
    it('Should try retry send to queue with parameter data is undefined', async () => {
      const to = 'bank-connector';
      const retry = 1;
      const data = undefined;

      const responseNockretrySaveUserBankDetails = {
        error: 'Input Parameter data not found.',
      };

      const nockSendToSqs = nocks.retry.queue.sendToSqs({
        statusCode: 400,
        errorMessage: responseNockretrySaveUserBankDetails,
      });

      const spyLoggerError = jest.spyOn(logger, 'error').mockImplementation();
      const messageLogError =
        'Error client retry save-user-bank-betails, error: {"error":"Input Parameter data not found."}';

      const result = await sendToSqs({ to, retry, data });

      expect(result.error).toStrictEqual(
        responseNockretrySaveUserBankDetails.error
      );
      expect(result).toHaveProperty('error');
      expect(nockSendToSqs.isDone()).toBeFalsy();
      expect(spyLoggerError).toHaveBeenCalledTimes(1);
      expect(spyLoggerError.mock.calls[0].toString()).toContain(
        messageLogError
      );
      spyLoggerError.mockRestore();
    });
  });
});

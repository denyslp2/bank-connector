require('dotenv').config();

const {
  postDocumentUserProfile,
} = require('../../../../../src/infrastructure/clients/user-profile');
const { logger } = require('../../../../../src/infrastructure/utils');
const { fixtures, nocks } = require('../../../../utils');

describe('Unit tests client user-profile, route POST /api/income_document_data_webhook', () => {
  beforeEach(() => {
    nocks.cleanAll();
    jest.clearAllMocks();
  });
  describe('Success cases', () => {
    it('Should send userBankDetails to user-profile with successful', async () => {
      const provider = 'belvo';
      const userUuid = 'uuid';
      const userBankDetails = {
        transaction: {
          instantorRequestId: 'uuid',
          userDetails: {},
          bankInfo: {},
          accountReportList: [],
          accountList: [
            {
              transactionList: [],
            },
          ],
        },
      };

      const userProfileFixture =
        fixtures.userProfile.incomeDocument.documentSaved();

      const nockPostUserProfile =
        nocks.userProfile.incomeDocument.postUserProfile({
          provider,
          userUuid,
          responseBody: userProfileFixture,
        });

      const result = await postDocumentUserProfile(userUuid, userBankDetails);

      expect(result.statusCode).toEqual(200);
      expect(result.body).toStrictEqual(userProfileFixture);
      expect(nockPostUserProfile.isDone()).toBeTruthy();
    });
  });
  describe('Error cases', () => {
    it('Should try post user-profile with userUuid invalid', async () => {
      const provider = 'belvo';
      const userUuid = 'invalid_uuid';
      const userBankDetails = {
        transaction: {
          instantorRequestId: 'uuid',
          userDetails: {},
          bankInfo: {},
          accountReportList: [],
          accountList: [
            {
              transactionList: [],
            },
          ],
        },
      };

      const responseNockPostUserProfile = {
        statusCode: 404,
        error: {
          error: 'Error saving IncomeDocumentDataBody: user_uuid not found',
        },
      };

      const nockPostUserProfile =
        nocks.userProfile.incomeDocument.postUserProfile({
          provider,
          userUuid,
          statusCode: 404,
          responseBody: responseNockPostUserProfile,
        });

      const spyLoggerError = jest.spyOn(logger, 'error').mockImplementation();
      const messageLogError =
        'Error user-profile client send documents, error: {"statusCode":404,"error":{"statusCode":404,"error":{"error":"Error saving IncomeDocumentDataBody: user_uuid not found"}}}';

      const result = await postDocumentUserProfile(userUuid, userBankDetails);

      expect(result.error).toStrictEqual(responseNockPostUserProfile);
      expect(result).toHaveProperty('error');
      expect(nockPostUserProfile.isDone()).toBeTruthy();
      expect(spyLoggerError).toHaveBeenCalledTimes(1);
      expect(spyLoggerError.mock.calls[0].toString()).toContain(
        messageLogError
      );
      spyLoggerError.mockRestore();
    });
    it('Should try post user-profile with undefined userUuid ', async () => {
      const userUuid = undefined;
      const userBankDetails = {
        transaction: {
          instantorRequestId: 'uuid',
          userDetails: {},
          bankInfo: {},
          accountReportList: [],
          accountList: [
            {
              transactionList: [],
            },
          ],
        },
      };

      const responseNockPostUserProfile = {
        error: 'Input Parameter userUuid not found.',
      };

      const spyLoggerError = jest.spyOn(logger, 'error').mockImplementation();
      const messageLogError =
        'Error user-profile client send documents, error: {"error":"Input Parameter userUuid not found."}';

      const result = await postDocumentUserProfile(userUuid, userBankDetails);

      expect(result.error).toStrictEqual(responseNockPostUserProfile.error);
      expect(result).toHaveProperty('error');
      expect(spyLoggerError).toHaveBeenCalledTimes(1);
      expect(spyLoggerError.mock.calls[0].toString()).toContain(
        messageLogError
      );
      spyLoggerError.mockRestore();
    });
    it('Should try post user-profile with undefined userBankDetails', async () => {
      const userUuid = 'uuid';
      const userBankDetails = undefined;

      const responseNockPostUserProfile = {
        error: 'Input Parameter userBankDetails not found.',
      };

      const spyLoggerError = jest.spyOn(logger, 'error').mockImplementation();
      const messageLogError =
        'Error user-profile client send documents, error: {"error":"Input Parameter userBankDetails not found."}';

      const result = await postDocumentUserProfile(userUuid, userBankDetails);

      expect(result.error).toStrictEqual(responseNockPostUserProfile.error);
      expect(result).toHaveProperty('error');
      expect(spyLoggerError).toHaveBeenCalledTimes(1);
      expect(spyLoggerError.mock.calls[0].toString()).toContain(
        messageLogError
      );
      spyLoggerError.mockRestore();
    });
  });
});

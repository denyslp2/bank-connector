require('dotenv').config();

const {
  getTransactions,
} = require('../../../../../src/infrastructure/clients/belvo-services');
const { logger } = require('../../../../../src/infrastructure/utils');
const { fixtures, nocks } = require('../../../../utils');

describe('Unit tests client belvo-services, route GET /transactions', () => {
  beforeEach(() => {
    nocks.cleanAll();
    jest.clearAllMocks();
  });
  describe('Success cases', () => {
    it('Should return transactions list to linkId and filters successful', async () => {
      const linkId = 'uuid';
      const dateFrom = '2021-01-01';
      const dateTo = '2021-03-01';
      const query = `value_date_range=${dateFrom},${dateTo}`;
      const transactionsFixture =
        fixtures.belvoServices.transactions.lastTransactions();
      const nockGetTransactions =
        nocks.belvoServices.transactions.getTransactions({
          linkId,
          query,
          responseBody: transactionsFixture,
        });

      const result = await getTransactions(linkId, query);

      expect(result.statusCode).toEqual(200);
      expect(result.body).toStrictEqual(transactionsFixture);
      expect(nockGetTransactions.isDone()).toBeTruthy();
    });
    it('Should return transactions list to linkId successful', async () => {
      const linkId = 'uuid';
      const transactionsFixture =
        fixtures.belvoServices.transactions.lastTransactions();
      const nockGetTransactions =
        nocks.belvoServices.transactions.getTransactions({
          linkId,
          responseBody: transactionsFixture,
        });

      const result = await getTransactions(linkId);

      expect(result.statusCode).toEqual(200);
      expect(result.body).toStrictEqual(transactionsFixture);
      expect(nockGetTransactions.isDone()).toBeTruthy();
    });
  });
  describe('Error cases', () => {
    it('Should return error when try get transactions without linkId', async () => {
      const transactionsFixture =
        fixtures.belvoServices.transactions.lastTransactions();
      const nockGetTransactions =
        nocks.belvoServices.transactions.getTransactions({
          responseBody: transactionsFixture,
        });
      const spyLoggerError = jest.spyOn(logger, 'error').mockImplementation();
      const errorMessage = `Input Parameter linkId not found.`;
      const messageLogError = `Error belvo-services client getTransactions, error: {"error":"${errorMessage}"}`;

      const result = await getTransactions();

      expect(result.error).toStrictEqual(errorMessage);
      expect(result).toHaveProperty('error');
      expect(nockGetTransactions.isDone()).toBeFalsy();
      expect(spyLoggerError).toHaveBeenCalledTimes(1);
      expect(spyLoggerError.mock.calls[0].toString()).toContain(
        messageLogError
      );
      spyLoggerError.mockRestore();
    });
    it('Should request belvo-services with return Unauthorized error.', async () => {
      const linkId = 'uuid';
      const dateFrom = '2021-01-01';
      const dateTo = '2021-03-01';
      const query = `value_date_range=${dateFrom},${dateTo}`;

      const responseNockGetTransactions = {
        data: 'Unauthorized on belvo-services',
        message: 'Unauthorized',
      };

      const spyLoggerError = jest.spyOn(logger, 'error').mockImplementation();
      const messageLogError =
        'Error belvo-services client getTransactions, error: {"statusCode":401,"error":{"data":"Unauthorized on belvo-services","message":"Unauthorized"}}';

      const nockGetTransactions =
        nocks.belvoServices.transactions.getTransactions({
          linkId,
          responseBody: responseNockGetTransactions,
          statusCode: 401,
          query,
        });

      const result = await getTransactions(linkId, query);

      expect(result.statusCode).toEqual(401);
      expect(result).toHaveProperty('error');
      expect(result.error).toStrictEqual(responseNockGetTransactions);
      expect(nockGetTransactions.isDone()).toBeTruthy();
      expect(spyLoggerError).toHaveBeenCalledTimes(1);
      expect(spyLoggerError.mock.calls[0].toString()).toContain(
        messageLogError
      );
      spyLoggerError.mockRestore();
    });
    it('Should return internal error belvo-services client when try get transactions without linkId', async () => {
      const linkId = 'uuid';
      const errorMessage = 'timeout';
      const nockGetTransactions =
        nocks.belvoServices.transactions.getTransactions({
          linkId,
          errorMessage,
        });
      const spyLoggerError = jest.spyOn(logger, 'error').mockImplementation();
      const messageLogError = `Error belvo-services client getTransactions, error: {"error":"${errorMessage}"}`;

      const result = await getTransactions(linkId);

      expect(result.error).toStrictEqual('timeout');
      expect(result).toHaveProperty('error');
      expect(nockGetTransactions.isDone()).toBeTruthy();
      expect(spyLoggerError).toHaveBeenCalledTimes(1);
      expect(spyLoggerError.mock.calls[0].toString()).toContain(
        messageLogError
      );
      spyLoggerError.mockRestore();
    });
  });
});

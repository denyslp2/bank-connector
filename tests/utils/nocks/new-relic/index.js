const nock = require('nock');

const sendEvent = (options = {}) => {
  const url = options.url || `${process.env.NEW_RELIC_URL}`;
  const accountId = options.accountId || process.env.NEW_RELIC_ACCOUNT_ID;
  const licenseKey = options.licenseKey || process.env.NEW_RELIC_LICENSE_KEY;
  const headers = {
    'X-License-Key': licenseKey,
    'Content-Type': 'application/json',
  };

  if (options.errorMessage) {
    return nock(url)
      .post(`/v1/accounts/${accountId}/events`, { interceptorOptions: headers })
      .replyWithError(400, options.errorMessage);
  }
  return nock(url)
    .post(`/v1/accounts/${accountId}/events`, { interceptorOptions: headers })
    .reply(options.statusCode || 200, {});
};

module.exports = {
  sendEvent,
};

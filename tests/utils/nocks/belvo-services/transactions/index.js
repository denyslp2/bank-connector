const nock = require('nock');

const getTransactions = (options = {}) => {
  const url = options.url || `${process.env.BELVO_SERVICES_API_URL}`;
  const path = options.query
    ? `/linkId/${options.linkId}/transactions/${options.query}`
    : `/linkId/${options.linkId}/transactions`;
  if (options.errorMessage) {
    return nock(url).get(path).replyWithError(options.errorMessage);
  }
  return nock(url)
    .get(path)
    .reply(options.statusCode || 200, options.responseBody || {});
};

module.exports = {
  getTransactions,
};

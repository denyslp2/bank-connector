const nock = require('nock');

const sendToSqs = (options = {}) => {
  const url = options.url || `${process.env.SQS_API_URL}`;
  const path = '/sqs-plutao-dev';
  if (options.errorMessage) {
    return nock(url).get(path).replyWithError(options.errorMessage);
  }
  return nock(url)
    .post(path)
    .reply(options.statusCode || 200, options.responseBody || {});
};

module.exports = {
  sendToSqs,
};

const nock = require('nock');

const postUserProfile = (options = {}) => {
  const url = options.url || `${process.env.USER_PROFILE_URL}`;
  const path = `/api/income_document_data_webhook/${options.provider}/${options.userUuid}`;

  if (options.errorMessage) {
    return nock(url).post(path).replyWithError(options.errorMessage);
  }
  return nock(url)
    .post(path)
    .reply(options.statusCode || 200, options.responseBody || {});
};

module.exports = {
  postUserProfile,
};

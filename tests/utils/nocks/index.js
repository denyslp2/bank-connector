const nock = require('nock');

const belvoServices = require('./belvo-services');
const newRelic = require('./new-relic');
const retry = require('./retry');
const userProfile = require('./user-profile');
const { cleanAll } = nock;

module.exports = {
  cleanAll,
  belvoServices,
  newRelic,
  userProfile,
  retry,
};

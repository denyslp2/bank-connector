const belvoServices = require('./belvo-services');
const userProfile = require('./user-profile');

module.exports = {
  belvoServices,
  userProfile,
};

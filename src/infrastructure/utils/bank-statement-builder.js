class BankStatementResultBuilder {
  constructor() {
    this.bankStatementResult = {
      id: '',
      account: {
        id: '',
        institution: {
          name: '',
          type: '',
        },
        created_at: '',
        name: '',
        number: '',
        balance: {
          current: '',
          available: '',
        },
        currency: '',
        internal_identification: '',
        public_identification_name: '',
        public_identification_value: '',
      },
    };
  }

  setId(value) {
    this.bankStatementResult.id = value;
    return this;
  }
  setAccountId(value) {
    this.bankStatementResult.account.id = value;
    return this;
  }
  setAccountInstitutionName(value) {
    this.bankStatementResult.account.institution.name = value;
    return this;
  }
  setAccountInstitutionType(value) {
    this.bankStatementResult.account.institution.type = value;
    return this;
  }
  setAccountCreatedAt(value) {
    this.bankStatementResult.account.created_at = value;
    return this;
  }
  setAccountName(value) {
    this.bankStatementResult.account.name = value;
    return this;
  }
  setAccountNumber(value) {
    this.bankStatementResult.account.number = value;
    return this;
  }
  setAccountBalanceCurrent(value) {
    this.bankStatementResult.account.balance.current = value;
    return this;
  }
  setAccountBalanceAvailable(value) {
    this.bankStatementResult.account.balance.available = value;
    return this;
  }
  setAccountCurrency(value) {
    this.bankStatementResult.account.currency = value;
    return this;
  }
  setAccountInternalIdentification(value) {
    this.bankStatementResult.account.internal_identification = value;
    return this;
  }
  setAccountPublicIdentificationName(value) {
    this.bankStatementResult.account.public_identification_name = value;
    return this;
  }
  setAccountPublicIdentificationValue(value) {
    this.bankStatementResult.account.public_identification_value = value;
    return this;
  }
  build() {
    return this.bankStatementResult;
  }
}

class BankStatementBuilder {
  constructor() {
    this.bankStatement = {
      count: 0,
      pagination: {
        next: '',
        previous: '',
      },
      results: [],
    };
  }

  setCount(value) {
    this.bankStatement.count = value;
    return this;
  }
  setPaginationNext(value) {
    this.bankStatement.pagination.next = value;
    return this;
  }
  setPaginationPrevious(value) {
    this.bankStatement.pagination.previous = value;
    return this;
  }
  setResult(value) {
    this.bankStatement.results = value;
    return this;
  }
  build() {
    return this.bankStatement;
  }
}

module.exports = {
  BankStatementResultBuilder,
  BankStatementBuilder,
};

class UserBankDetailsBuilder {
  constructor() {
    this.userBankDetails = {
      transaction: {
        instantorRequestId: '',
        userDetails: {
          name: '',
          address: '',
          phone: '',
          email: '',
          personalIdentifier: [
            {
              name: 'cpf',
              value: '',
            },
          ],
        },
        bankInfo: {
          name: '',
          country: 'br',
          id: '',
        },
        accountReportList: [
          {
            number: '',
          },
        ],
        accountList: [
          {
            number: '',
            currency: '',
            balance: '',
            availableAmount: '',
            transactionList: [
              {
                onDate: '',
                description: '',
                amount: '',
                balance: '',
              },
            ],
          },
        ],
      },
    };
  }

  setTransactionInstantorRequestId(value) {
    this.userBankDetails.transaction.instantorRequestId = value || '';
    return this;
  }
  setTransactionUserDetailsName(value) {
    this.userBankDetails.transaction.userDetails.name = value || '';
    return this;
  }
  setTransactionUserDetailsAddress(value) {
    this.userBankDetails.transaction.userDetails.address = value || '';
    return this;
  }
  setTransactionUserDetailsPhone(value) {
    this.userBankDetails.transaction.userDetails.phone = value || '';
    return this;
  }
  setTransactionUserDetailsEmail(value) {
    this.userBankDetails.transaction.userDetails.email = value || '';
    return this;
  }
  setTransactionUserDetailsPersonalIdentifierCPF(value) {
    this.userBankDetails.transaction.userDetails.personalIdentifier[0].value =
      value || '';
    return this;
  }
  setTransactionBankInfoName(value) {
    this.userBankDetails.transaction.bankInfo.name = value || '';
    return this;
  }
  setTransactionBankInfoId(value) {
    this.userBankDetails.transaction.bankInfo.id = value || '';
    return this;
  }
  setTransactionAccountReportListNumber(value) {
    this.userBankDetails.transaction.accountReportList[0].number = value || '';
    return this;
  }
  setTransactionAccountListNumber(value) {
    this.userBankDetails.transaction.accountList[0].number = value || '';
    return this;
  }
  setTransactionAccountListCurrency(value) {
    this.userBankDetails.transaction.accountList[0].currency = value || '';
    return this;
  }
  setTransactionAccountListBalance(value) {
    this.userBankDetails.transaction.accountList[0].balance = value || '';
    return this;
  }
  setTransactionAccountListAvailableAmount(value) {
    this.userBankDetails.transaction.accountList[0].availableAmount =
      value || '';
    return this;
  }
  setTransactionAccountListTransactionList(value) {
    this.userBankDetails.transaction.accountList[0].transactionList =
      value || '';
    return this;
  }
  build() {
    return this.userBankDetails;
  }
}

class UserBankDetailsTransactionBuilder {
  constructor() {
    this.userBankDetailsTransaction = {
      onDate: '',
      description: '',
      amount: '',
      balance: '',
    };
  }

  setTransactionOnDate(value) {
    this.userBankDetailsTransaction.onDate = value || '';
    return this;
  }
  setTransactionDescription(value) {
    this.userBankDetailsTransaction.description = value || '';
    return this;
  }
  setTransactionAmount(value) {
    this.userBankDetailsTransaction.amount = value || '';
    return this;
  }
  setTransactionBalance(value) {
    this.userBankDetailsTransaction.balance = value || '';
    return this;
  }
  build() {
    return this.userBankDetailsTransaction;
  }
}

module.exports = {
  UserBankDetailsBuilder,
  UserBankDetailsTransactionBuilder,
};

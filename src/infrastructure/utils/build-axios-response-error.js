module.exports = (error) => {
  if (error.response) {
    return {
      statusCode: error.response.status,
      error: error.response.data,
    };
  }
  return {
    error: error.message,
  };
};

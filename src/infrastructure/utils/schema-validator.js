let Ajv = require('ajv');
const ajv = new Ajv();

const logger = require('./logger');

const schemaValidator = (() => {
  const validate = (data, schema) => {
    const schemaCompiled = ajv.compile(schema);
    const result = schemaCompiled(data);

    if (!result) {
      let errorMessage = `Error to validate schema from request: ${JSON.stringify(
        data
      )}`;
      schemaCompiled.errors.forEach((element) => {
        if (element.dataPath) {
          errorMessage += ` - field: ${element.dataPath} - error: ${element.message}`;
        } else {
          errorMessage += ` - error: ${element.message}`;
        }
      });
      logger.error(errorMessage);
    }

    return result;
  };

  return { validate };
})();

module.exports = schemaValidator;

const jwt = require('koa-jwt');

const isAuthenticated = jwt({
  secret: process.env.TOKEN,
  algorithm: 'HS256',
});

module.exports = {
  isAuthenticated,
};

const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const helmet = require('koa-helmet');
const Router = require('koa-router');

const router = new Router();

const cors = require('@koa/cors');

const packages = require('../../../package.json');
const { bankStatementRoutes } = require('../../bank-statement');
const { healthStatusRoutes } = require('../../health-status');
const { saveUserBankDetailsRoutes } = require('../../save-user-bank-details');
const { logger } = require('../utils');

class Server {
  constructor() {
    this.port = process.env.PORT || 3000;
    this.packages = packages;
    this.app = new Koa();
    this.router = router;
    this.defineRoutes();
    this.defineConfig();
  }

  defineConfig() {
    this.app.use(
      cors({
        allowedOrigins: '*',
        headers: '*',
      })
    );
    this.app.use(
      bodyParser({
        jsonLimit: '100mb',
      })
    );
    this.app.use(helmet());
    this.app.use(this.router.routes());
  }

  defineRoutes() {
    healthStatusRoutes(this.router);
    bankStatementRoutes(this.router);
    saveUserBankDetailsRoutes(this.router);
  }

  start() {
    return new Promise((resolve) => {
      this.httpServer = this.app.listen(this.port, () => {
        logger.info(
          '------------------------------------------------------------------'
        );
        logger.info(
          `${this.packages.name} - Version: ${this.packages.version}`
        );
        logger.info(
          '------------------------------------------------------------------'
        );
        logger.info(`Koa server listening on port: ${this.port}`);
        logger.info(
          '------------------------------------------------------------------'
        );
        return resolve(this.httpServer);
      });
    });
  }

  returnApp() {
    return this.app;
  }

  stop() {
    return new Promise((resolve) => {
      if (this.httpServer) {
        return this.httpServer.close(resolve);
      }
      return resolve();
    });
  }
}
module.exports = Server;

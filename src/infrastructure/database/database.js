const Knex = require('knex');

const knexConfig = require('../../../knexfile');

class Db {
  constructor() {
    this.db = null;
  }

  async connect() {
    const self = this;

    if (self.db) {
      return Promise.resolve();
    }

    const dbConfig = knexConfig[process.env.ENVIRONMENT || 'local_docker_db'];
    this.db = Knex(dbConfig);

    await this.db.raw('select 1+1 as result').catch((err) => {
      throw err;
    });
    return this.db;
  }

  async close() {
    if (this.db) {
      await this.db.destroy();
      this.client = null;
      this.db = null;
    }
  }
}

module.exports = Db;

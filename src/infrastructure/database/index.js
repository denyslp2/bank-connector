/* eslint-disable */
const { logger } = require('../utils');
const Db = require('./database');

const dbInstance = new Db();

module.exports = {
  async close() {
    try {
      if (dbInstance) {
        logger.info('[Knex] Database trying to disconnect');
        await dbInstance.close();
      }
    } catch (e) {
      logger.error('Error on close DB: %j', e);
      throw e;
    }
  },
  async connect(uri) {
    try {
      // await dbInstance.connect(uri);
      // logger.info('[Knex] Database connected');
    } catch (e) {
      logger.error('[Knex] Database failed to connect - ', e.message);
      throw e;
    }
  },
  dbInstance,
};

const axios = require('axios');

const { logger, buildAxiosResponseError } = require('../../utils');

module.exports = async (payload) => {
  const { to, retry, data } = payload;
  try {
    const { SQS_API_URL, SQS_API_PATH } = process.env;

    if (!to) {
      throw new Error(`Input Parameter to not found.`);
    }

    if (!Number.isInteger(retry)) {
      throw new Error(`Input Parameter retry not a number.`);
    }

    if (!data) {
      throw new Error(`Input Parameter data not found.`);
    }

    const url = `${SQS_API_URL}/${SQS_API_PATH}`;

    const body = {
      to,
      retry,
      data,
    };

    const { data: retrySqs, status: statusCode } = await axios.post(url, body);

    if (statusCode !== 200) {
      throw new Error('Fail request to send to SQS.');
    }

    return {
      statusCode,
      body: retrySqs,
    };
  } catch (error) {
    const errorBuild = buildAxiosResponseError(error);

    logger.error(
      `Error client sendToSqs, error: ${JSON.stringify(errorBuild)}`
    );
    return errorBuild;
  }
};

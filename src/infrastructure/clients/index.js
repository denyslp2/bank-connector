const belvoServices = require('./belvo-services');
const newRelic = require('./new-relic');

module.exports = {
  newRelic,
  belvoServices,
};

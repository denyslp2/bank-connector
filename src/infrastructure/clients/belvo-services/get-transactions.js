const axios = require('axios');

const {
  logger,
  buildAxiosResponseError,
} = require('../../../infrastructure/utils');

module.exports = async (linkId, query) => {
  try {
    let queryString;
    if (!linkId) {
      throw new Error(`Input Parameter linkId not found.`);
    }
    queryString = query ? `/${query}` : '';
    const url = `${process.env.BELVO_SERVICES_API_URL}/linkId/${linkId}/transactions${queryString}`;

    const options = {
      headers: { Authorization: `Bearer ${process.env.BELVO_SERVICES_TOKEN}` },
    };

    const transactions = await axios.get(url, options);
    if (transactions.status !== 200) {
      throw new Error(`Input Parameter linkId not found.`);
    }

    return {
      statusCode: transactions.status,
      body: transactions.data,
    };
  } catch (error) {
    const errorBuild = buildAxiosResponseError(error);
    logger.error(
      `Error belvo-services client getTransactions, error: ${JSON.stringify(
        errorBuild
      )}`
    );
    return errorBuild;
  }
};

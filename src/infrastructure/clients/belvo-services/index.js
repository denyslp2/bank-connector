const getTransactions = require('./get-transactions');

module.exports = {
  getTransactions,
};

const axios = require('axios');

const {
  logger,
  buildAxiosResponseError,
} = require('../../../infrastructure/utils');

module.exports = async (userUuid, userBankDetails) => {
  try {
    const { USER_PROFILE_URL, USER_PROFILE_TOKEN } = process.env;

    if (!userUuid) {
      throw new Error(`Input Parameter userUuid not found.`);
    }

    if (!userBankDetails) {
      throw new Error(`Input Parameter userBankDetails not found.`);
    }

    const url = `${USER_PROFILE_URL}/api/income_document_data_webhook/belvo/${userUuid}`;

    const options = {
      headers: { Authorization: USER_PROFILE_TOKEN },
    };

    const { data: userProfile, status: statusCode } = await axios.post(
      url,
      userBankDetails,
      options
    );

    if (statusCode !== 200) {
      throw new Error('Fail request to user-profile.');
    }

    return {
      statusCode,
      body: userProfile,
    };
  } catch (error) {
    const errorBuild = buildAxiosResponseError(error);

    logger.error(
      `Error user-profile client send documents, error: ${JSON.stringify(
        errorBuild
      )}`
    );
    return errorBuild;
  }
};

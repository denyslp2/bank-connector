const axios = require('axios');

const {
  logger,
  buildAxiosResponseError,
} = require('../../../infrastructure/utils');

const sendEvent = async (event) => {
  try {
    const accountId = process.env.NEW_RELIC_ACCOUNT_ID;
    const licenseKey = process.env.NEW_RELIC_LICENSE_KEY;
    const url = `${process.env.NEW_RELIC_URL}/v1/accounts/${accountId}/events`;
    const body = event;
    const options = {
      headers: {
        'X-License-Key': licenseKey,
        'Content-Type': 'application/json',
      },
    };
    const result = await axios.post(url, body, options);
    return result;
  } catch (error) {
    const errorBuild = buildAxiosResponseError(error);
    logger.error(
      'Error new relic client sendEvent, code:%s, error:%o',
      errorBuild.statusCode,
      errorBuild
    );
    return errorBuild;
  }
};

module.exports = {
  sendEvent,
};

const {
  isAuthenticated,
} = require('../infrastructure/authorization/authorization-bearer-strategy');
const { getBankStatementController } = require('./controllers');
const requestsValidator = require('./validators/bank-statement-requests-validator');

const routes = (router) => {
  router.post(
    '/process-bank-statement',
    isAuthenticated,
    requestsValidator.validatePostRequest,
    getBankStatementController
  );
};

module.exports = routes;

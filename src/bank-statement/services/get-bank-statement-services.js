const { sendToSqs } = require('../../infrastructure/clients/sqs');
const regexDatePatterns =
  /^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/; //eslint-disable-line

module.exports = async (credentials = {}, filters = {}) => {
  const payload = {
    to: 'belvo-service',
    retry: 0,
    data: {
      userUuid: credentials.userUuid,
      linkId: credentials.linkId,
      dateFrom: filters.dateFrom || '',
      dateTo: filters.dateTo || '',
    },
  };

  if (filters) {
    if (filters.dateTo && !filters.dateTo.match(regexDatePatterns)) {
      return {
        statusCode: 400,
        body: 'Bad Request - invalid date to',
      };
    } else if (filters.dateFrom && !filters.dateFrom.match(regexDatePatterns)) {
      return {
        statusCode: 400,
        body: 'Bad Request - invalid date from',
      };
    }
  }

  return sendToSqs(payload).catch((err) => {
    return {
      statusCode: 500,
      body: JSON.stringify(err),
    };
  });
};

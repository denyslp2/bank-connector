const { logger } = require('../../infrastructure/utils');
const { getBankStatementService } = require('../services');

module.exports = async (ctx) => {
  const { request } = ctx;
  const { credentials, filters } = request.body;

  try {
    const { statusCode, body } = await getBankStatementService(
      credentials,
      filters
    );
    if (statusCode != 200) {
      ctx.status = statusCode;
      ctx.body = body;
      return ctx;
    }
    logger.info(
      `post bank statement partner on queue | credentials:${JSON.stringify(
        credentials
      )} successfully.`
    );
    ctx.status = 200;
    return ctx;
  } catch (error) {
    logger.error('Error get statement controller error: %o', error);
    ctx.status = 500;
    return ctx;
  }
};

const ip = require('ip');

const pkg = require('../../package.json');

const healthStatusController = (() => {
  const get = (ctx) => {
    const health = {
      datetime: new Date(),
      service: pkg.name,
      version: pkg.version,
      ip: ip.address(),
      container: process.env.HOSTNAME,
    };

    ctx.status = 200;
    ctx.body = health;
    return ctx;
  };

  return {
    get,
  };
})();

module.exports = healthStatusController;

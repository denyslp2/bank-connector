const schemaValidator = require('../../infrastructure/utils/schema-validator');
const requestsSchema = require('./post-save-user-bank-details-request-schema.json');

const saveUserBankDetailsRequestsValidator = (() => {
  const validatePostRequest = (ctx, next) => {
    const schemaIsValid = schemaValidator.validate(
      ctx.request.body,
      requestsSchema
    );

    if (!schemaIsValid) {
      ctx.status = 400;
      ctx.body = 'Invalid body request';
      return ctx;
    }

    return next();
  };

  return { validatePostRequest };
})();

module.exports = saveUserBankDetailsRequestsValidator;

const { sendToSqs } = require('../../infrastructure/clients/sqs');
const {
  postDocumentUserProfile,
} = require('../../infrastructure/clients/user-profile');
const { logger } = require('../../infrastructure/utils');
const {
  UserBankDetailsBuilder,
  UserBankDetailsTransactionBuilder,
} = require('../../infrastructure/utils/user-bank-details-builder');

module.exports = async (data, to = 'bank-connector', retry = 0) => {
  const { MAX_RETRY_SQS } = process.env;

  let { owner_data, transactions_data } = data;
  const { userUuid } = data;

  transactions_data = transactions_data || [];
  owner_data = owner_data || [];

  const [ownerDetails] = owner_data;
  const [transactionsDetails] = transactions_data;

  const currentAccount = transactions_data?.find(
    (transaction) =>
      transaction?.account?.name?.toUpperCase() === 'CONTA CORRENTE'
  );

  const userBankDetailsTransactions = transactions_data?.map((transaction) => {
    const userBankDetailsTransactionBuilder =
      new UserBankDetailsTransactionBuilder();

    const sinal = transaction?.type === 'INFLOW' ? '+' : '-';

    return userBankDetailsTransactionBuilder
      .setTransactionOnDate(transaction?.value_date)
      .setTransactionDescription(transaction?.description)
      .setTransactionAmount(`${sinal}${transaction?.amount}`)
      .setTransactionBalance(transaction?.balance)
      .build();
  });

  const firstName = ownerDetails?.first_name || '';
  const lastName = ownerDetails?.last_name || '';
  const userName = ownerDetails?.display_name || `${firstName} ${lastName}`;

  const userBankDetailsBuilder = new UserBankDetailsBuilder();
  const userBankDetails = userBankDetailsBuilder
    .setTransactionInstantorRequestId(ownerDetails?.link)
    .setTransactionUserDetailsName(userName.trim())
    .setTransactionUserDetailsAddress(ownerDetails?.address)
    .setTransactionUserDetailsPhone(ownerDetails?.phone_number)
    .setTransactionUserDetailsEmail(ownerDetails?.email)
    .setTransactionUserDetailsPersonalIdentifierCPF(
      ownerDetails?.document_id?.document_number
    )
    .setTransactionBankInfoName(transactionsDetails?.account?.institution?.name)
    .setTransactionBankInfoId(transactionsDetails?.account?.id)
    .setTransactionAccountReportListNumber(
      currentAccount?.account?.public_identification_value
    )
    .setTransactionAccountListNumber(
      currentAccount?.account?.public_identification_value
    )
    .setTransactionAccountListCurrency(transactionsDetails?.account?.currency)
    .setTransactionAccountListBalance(
      transactionsDetails?.account?.balance?.current
    )
    .setTransactionAccountListAvailableAmount(
      transactionsDetails?.account?.balance?.available
    )
    .setTransactionAccountListTransactionList(userBankDetailsTransactions)
    .build();

  const userProfileResponse = await postDocumentUserProfile(
    userUuid,
    userBankDetails
  );

  if (userProfileResponse && userProfileResponse.statusCode !== 200) {
    retry += 1;

    if (retry <= MAX_RETRY_SQS) {
      logger.error(
        `PostDocumentUserProfile failed retry process called sendToSqs | error: ${JSON.stringify(
          userProfileResponse
        )}`
      );
      const payload = {
        to,
        retry,
        data,
      };
      return sendToSqs(payload);
    }
  }

  return userProfileResponse;
};

const { logger } = require('../../infrastructure/utils');
const { saveUserBankDetailsService } = require('../services');

module.exports = async (ctx) => {
  const { request } = ctx;
  const { to, retry, data } = request.body;

  try {
    saveUserBankDetailsService(data, to, retry);

    logger.info(
      `Post successfully to save User Bank Details To: ${to} | Retry: ${retry}`
    );

    ctx.status = 200;
    ctx.body = { message: 'success' };
    return ctx;
  } catch (error) {
    logger.error('Error save user bank details controller error: %o', error);
    ctx.status = 500;
    return ctx;
  }
};

const {
  isAuthenticated,
} = require('../infrastructure/authorization/authorization-bearer-strategy');
const { saveUserBankDetailsController } = require('./controllers');
const {
  validatePostRequest,
} = require('./validators/save-user-bank-details-requests-validator');

const routes = (router) => {
  router.post(
    '/save-user-bank-details',
    isAuthenticated,
    validatePostRequest,
    saveUserBankDetailsController
  );
};

module.exports = routes;

# API Bank Connector

## Description

REST API project made with NodeJS, based on Provu's [NodeJS project model](https://bitbucket.org/lendicobrasil/project-model-nodejs), responsible for connecting with bank data providers

## Features

- [x] Project base
- [x] Add provider [Belvo service](https://bitbucket.org/lendicobrasil/belvo-services)
- [ ] Add Docker configuration

## Technologies implemented

- [koa](https://github.com/koajs/koa)
- [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken)
- [knex](https://github.com/knex/knex)
- [winston](https://github.com/winstonjs/winston)
- [jest](https://jestjs.io/)
- [supertest](https://github.com/visionmedia/supertest)
- [nock](https://github.com/nock/nock)
- [chance](https://github.com/chancejs/chancejs)
- [axios](https://github.com/axios/axios)

## Prerequisites

- [Node.js](https://github.com/nodejs/node)
- [npm](https://github.com/npm/cli)

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start
```

## Test

```bash
# test coverage
$ npm run test
```

## Lint

```bash
# lint fix
$ npm run lint:fix
```

## Directories

```bash
$ tree

├── .build # Scripts to configuration and run Docker
├── dockerfiles # Docker configuration files
├── docs # Collections postman
├── scripts # Scripts to install and start Docker
├── src
│   ├── bank-statement # Get bank statement from banks
│   │   ├── controllers
│   │   ├── services
│   │   └── validators
│   ├── health-status # Current server health status
│   ├── infrastructure # General settings, current server, clients, authentication
│   │   ├── authorization
│   │   ├── clients
│   │   ├── database
│   │   ├── server
│   │   └── utils
│   └── save-user-bank-details # Save bank data retrieved from providers
│       ├── controllers
│       ├── services
│       └── validators
└── tests
    ├── integrations # Integration tests with exposed routes
    ├── unit
    │   └── infrastructure
    │       └── clients # Unit tests with clients
    └── utils
        ├── fixtures # Payload mock
        └── nocks # Requests mock
```

## Documentation

Documentation for using the API Bank Connector

- [x] [Docs Endpoints](/docs)
- [x] Collection postman

## Docker configuration

### (Docker) Prerequisites

- [Docker](https://docs.docker.com)

### (Docker) Setting up the database for development and test

PostgreSQL database connection options are shown in the following table

| Option        | Development       | Test          |
| ------------- | ----------------- | ------------- |
| Host          | localhost         | localhost     |
| Port          | 5432              | 5432          |
| Username      | postgres          | postgres      |
| Password      | lendico123        | lendico123    |
| Database      | project-model     | project-model |
| ENVIRONMENT   | local_docker_db   | development   |

### (Docker) Installation

```bash
$ npm install
$ npm run migrations
$ npm run seeds
```

### (Docker) Running the app

```bash
# docker
$ docker-compose up
```

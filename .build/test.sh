#!/bin/bash

# exit when any command fails
set -xeuT

# load the generic functions
source build-functions.sh

# load the build variables
source ./.build/dotenv.sh

#buildTestsAndReport \
#    $IMAGE_NAME \
#    $DOCKER_ROOT \
#    $DOCKER_FILE

docker-compose -f tests-docker-compose.yml up --exit-code-from node-app
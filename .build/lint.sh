#!/bin/bash

# exit when any command fails
set -euT

# load the build variables
source ./.build/dotenv.sh

# load the generic functions
source build-functions.sh

runSonarLint $DOCKER_ROOT

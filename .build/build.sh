#!/bin/bash

# exit when any command fails
set -euT

# load the generic functions
source build-functions.sh

# load the build variables
source ./.build/dotenv.sh

# upload image
buildAndUploadServiceToEcr \
    $IMAGE_NAME \
    $DOCKER_ROOT \
    $DOCKER_FILE

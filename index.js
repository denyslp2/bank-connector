require('dotenv').config();
require('newrelic');
const pkg = require('./package.json');
const Server = require('./src/infrastructure/server');
const { logger } = require('./src/infrastructure/utils');

process.title = pkg.name;

const instanceServer = new Server();

const shutdown = async () => {
  logger.info('Gracefully shutdown in progress');
  await instanceServer.stop();
  process.exit(0);
};

process
  .on('SIGTERM', shutdown)
  .on('SIGINT', shutdown)
  .on('SIGHUP', shutdown)
  .on('uncaughtException', (err) => {
    logger.error('uncaughtException caught the error: ', err);
    throw err;
  })
  .on('unhandledRejection', (err, promise) => {
    logger.error(`Unhandled Rejection at: Promise ${promise} reason: ${err}`);
    throw err;
  })
  .on('exit', (code) => {
    logger.info(`Node process exit with code: ${code}`);
  });

(async () => {
  try {
    await instanceServer.start();
  } catch (err) {
    logger.error('[APP] initialization failed', err);
    throw err;
  }
  logger.info('[APP] initialized SUCCESSFULLY');
})();

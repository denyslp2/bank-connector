# Docs - API Bank Connector

[Voltar](/)

## Description

Documentation for using the API Bank Connector

## Postman

Postman collection to consume API endpoints

[JSON Collection](/docs/postman/API%20Bank%20Connector.postman_collection.json)

## Endpoints

### GET /health-status

- Check current server status

#### Response

```json
// Success
Status 200
Body
{
    "datetime": "datetime",
    "service": "string",
    "version": "string",
    "ip": "string",
    "container": "string"
}
```

### POST /process-bank-statement

- Start bank statement request

#### Body

| Name |Required | Type
| - | - | -
| `credentials` | required | object
| `filters` | optional | object

`credentials:`

| Name | Required | Type
| - | - | -
| linkId | required | string
| userUuid | required | string

`filters:`

| Name | Required | Type
| - | - | -
| dateFrom | required | datetime
| dateTo | required | datetime

#### Response

```json
// Success
Status 200
Body "string"
```

```json
// Bad Request
Status 400
Body "string"
```

```json
// Unauthorized
Status 401
```

```json
// Error
Status 500
Body "string"
```

### POST /save-user-bank-details

- Save bank details

#### Body

| Name |Required | Type
| - | - | -
| `to` | required | string
| `retry` | required | number
| `data` | required | object

`data:`

| Name | Required | Type
| - | - | -
| userUuid | required | string
| owner_data | required | string
| transactions_data | required | string

#### Response

```json
// Success
Status 200
Body
{
    "message": "string"
}
```

```json
// Error
Status 500
```

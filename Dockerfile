FROM 178164760726.dkr.ecr.us-east-1.amazonaws.com/lendico.nodejs14-base:v0.1.0

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000
CMD [ "node", "index.js" ]
